//This file is part of gobitmex
//
//
//gobitmex is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation version 3 of the License.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//
// Copyright (C) 2018 Pierre NN

package gobitmex

import (
	"time"
)

//Order encodes all received orders (sent through a buffered channel Order)
type Order struct {
	OrderID               string    `json:"orderID,omitempty"`
	ClOrdID               string    `json:"clOrdID,omitempty"`
	ClOrdLinkID           string    `json:"clOrdLinkID,omitempty"`
	Account               int       `json:"account,omitempty"`
	Symbol                string    `json:"symbol,omitempty"`
	Side                  string    `json:"side,omitempty"`
	SimpleOrderQty        int64     `json:"simpleOrderQty,omitempty"`
	OrderQty              int64     `json:"orderQty,omitempty"`
	Price                 float32   `json:"price,omitempty"`
	DisplayQty            int64     `json:"displayQty,omitempty"`
	StopPx                float32   `json:"stopPx,omitempty"`
	PegOffsetValue        float32   `json:"pegOffsetValue,omitempty"`
	PegPriceType          string    `json:"pegPriceType,omitempty"`
	Currency              string    `json:"currency,omitempty"`
	SettlCurrency         string    `json:"settlCurrency,omitempty"`
	OrdType               string    `json:"ordType,omitempty"`
	TimeInForce           string    `json:"timeInForce,omitempty"`
	ExecInst              string    `json:"execInst,omitempty"`
	ContingencyType       string    `json:"contingencyType,omitempty"`
	ExDestination         string    `json:"exDestination,omitempty"`
	OrdStatus             string    `json:"ordStatus,omitempty"`
	Triggered             string    `json:"triggered,omitempty"`
	WorkingIndicator      bool      `json:"workingIndicator,omitempty"`
	OrdRejReason          string    `json:"ordRejReason,omitempty"`
	SimpleLeavesQty       uint32    `json:"simpleLeavesQty,omitempty"`
	LeavesQty             uint32    `json:"leavesQty,omitempty"`
	SimpleCumQty          uint32    `json:"simpleCumQty,omitempty"`
	CumQty                uint32    `json:"cumQty,omitempty"`
	AvgPx                 float32   `json:"avgPx,omitempty"`
	MultiLegReportingType string    `json:"multiLegReportingType,omitempty"`
	Text                  string    `json:"text,omitempty"`
	TransactTime          time.Time `json:"transactTime,omitempty"`
	Timestamp             time.Time `json:"timeStamp,omitempty"`
}

//Trade encodes all received orders (sent through a buffered channel Trade)
//If we registered to OrderBookL2_b and Trade_b on the same ws stream, then we
//fill the HiddenSize variable, else ignore it.
type Trade struct {
	Timestamp       time.Time `json:"timestamp,omitempty"`
	Symbol          string    `json:"symbol,omitempty"`
	Side            string    `json:"side,omitempty"`
	Size            uint32    `json:"size,omitempty"`
	Price           float32   `json:"price,omitempty"`
	TickDirection   string    `json:"tickDirection,omitempty"`
	TrdMatchID      string    `json:"trdMatchID,omitempty"`
	GrossValue      int       `json:"grossValue,omitempty"`
	HomeNotional    float32   `json:"homeNotional,omitempty"`
	ForeignNotional float32   `json:"foreignNotional,omitempty"`
}

//PriceLevel descripes simple price levels information on 128 bits.
//We could also have something like `map[Id]->(Size,Price)` as a data structure as well,
//but it would probably not match most OB implementations
type PriceLevel struct {
	ID    uint64  `json:"id,omitempty"`    //an arbitrary id returned by bitmex associated to a unique price
	Size  uint32  `json:"size,omitempty"`  //amount of contracts for a given Id
	Price float32 `json:"price,omitempty"` //price for a given Id
}

//OBUpdate encodes order book updates data directly received from the server.
type OBUpdate struct {
	Symbol string `json:"symbol,omitempty"`
	Side   string `json:"side,omitempty"`
	PriceLevel
}

//OrderBookHandler is an interface to connect your own OB to the Bitmex datafeed.
//See PriceLevel documentation for the meaning of id,size,price.
//
//See SimpleOB for an simple example of order book implementation.
type OrderBookHandler interface {
	//BitmexInsert: insert a new price level in the OB, should return false if failed
	BitmexInsert(isSell bool, id uint64, size uint32, price float32) bool

	//BitmexDelete: delete a price level with id id in the OB, should return false if failed
	BitmexDelete(isSell bool, id uint64) bool

	//BitmexUpdate: update the size of a price level with id id, should return false if failed
	BitmexUpdate(isSell bool, id uint64, size uint32) bool

	//BitmexFill: fill (initialize/reset) the whole OB
	BitmexFill(levels []OBUpdate) error
}
