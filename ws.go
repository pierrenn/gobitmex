//This file is part of gobitmex
//
//
//gobitmex is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation version 3 of the License.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//
// Copyright (C) 2018 Pierre NN

package gobitmex

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/url"
	"os"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"github.com/gorilla/websocket"
)

const wsPingPeriod = 5

//StreamID is a bitmask representing which data subscriptions are associated
//with a websocket stream
type StreamID uint8

//StreamID is a list of ws streams we can subscribe to.
//This, matched with a symbol, uniquely describes a stream.
const (
	//0 is no stream
	SetOrderbookL2 StreamID = 1 << iota //OB L2 data streaming
	SetTrade                            //Last trade data streaming
	SetOrder                            //User orders data streaming
)

//represent a websocket stream
//note that one websocket stream can fill no/one/multiple data channel
type wsstream struct {
	logger *log.Logger
	stream *websocket.Conn

	isAuthenticated   bool
	subscribedStreams uint64 //all streams we are subscribed to
	chanSize          int    //channel size passed as argument when subscribing to thee stream
	symbol            string //select a given symbol; should be empty if not usable

	//detect if a new messages (data + pong answer) has been seen, reset to false every ticker tick
	//we send a ping to the server every tick of the ticker so isThereNewMessage stays alive
	isThereNewMessage uint32
	ticker            *time.Ticker
	hasFinished       chan struct{}
}

//make a WSStream object for the URL/symbol given as parameter
func newStream(l *log.Logger, mexurl string, chanSize int, symb string) (*wsstream, error) {
	u := url.URL{Scheme: "wss", Host: mexurl + ":443", Path: "/realtime"}
	ret, resp, err := websocket.DefaultDialer.Dial(u.String(), nil) //just ignore the http response
	if err != nil {
		if err == websocket.ErrBadHandshake {
			return nil, fmt.Errorf("handshake failed with status %d", resp.StatusCode)
		}

		return nil, err
	}

	stream := &wsstream{
		logger:          l,
		stream:          ret,
		chanSize:        chanSize,
		symbol:          symb,
		isAuthenticated: false,
		ticker:          nil,
		hasFinished:     make(chan struct{}, 1),
	}

	//read welcome message
	_, message, err := stream.stream.ReadMessage()
	if err != nil {
		return nil, fmt.Errorf("error reading welcome message %v", err)
	}

	//TODO: add more tests
	var data struct {
		Docs string `json:"docs"`
	}
	if err := json.NewDecoder(bytes.NewReader(message)).Decode(&data); err != nil {
		return nil, fmt.Errorf("could not decode mex welcome message: %v", err)
	}

	//check the doc api hasn't changed
	if data.Docs != ("https://" + mexurl + "/app/wsAPI") {
		return nil, fmt.Errorf("websocket document location has been updated, please contact the library author")
	}

	atomic.StoreUint32(&stream.isThereNewMessage, 0)
	atomic.StoreUint64(&stream.subscribedStreams, 0)
	return stream, nil
}

//authenticate a WSStream object
func (wss *wsstream) authenticate(publicKey, privateKey string) error {
	signatureTimerLock.Lock()
	wss.stream.WriteMessage(websocket.TextMessage, []byte(wsStreamAuthenticate(publicKey, privateKey)))
	signatureTimerLock.Unlock()
	_, msg, err := wss.stream.ReadMessage()
	if err != nil {
		return fmt.Errorf("api response reading: %v", err)
	}

	var data struct {
		Success bool `json:"Success"`
	}
	if err := json.NewDecoder(bytes.NewReader(msg)).Decode(&data); err != nil {
		return fmt.Errorf("api response decoding: %v", err)
	}

	if !data.Success {
		return fmt.Errorf("success to false: %s", msg)
	}

	wss.isAuthenticated = true
	return nil
}

//subscribe to a list of streams for a given conncection.
//parameter is a list of comma separated quoted stream names
func (wss *wsstream) subscribe(args string) error {
	wss.stream.WriteMessage(websocket.TextMessage, []byte("{\"op\": \"subscribe\", \"args\": ["+args+"]}"))

	for k := 0; k < strings.Count(args, ",")+1; k++ { //we receive as many subscribe message as there is commas in args
		_, subscribed, err := wss.stream.ReadMessage()
		if err != nil {
			return fmt.Errorf("Error while reading subscription ack message from the orders ws stream: %v , will not report orders", err)
		}

		var data struct {
			Success bool   `json:"success"`
			Error   string `json:"error"`
		}
		if err := json.NewDecoder(bytes.NewReader(subscribed)).Decode(&data); err != nil {
			return fmt.Errorf("could not decode order message: %v", err)
		}

		if !data.Success || data.Error != "" {
			return fmt.Errorf("server answered [%s]", subscribed)
		}
	}

	return nil
}

//start a ticker to send ping/pong to the bitmex server: should be activated just before
//we start the reader loop.
//every ws_ping_period seconds, we check if a message has been received (isThereNewMessage)
//if none, we send a ping message: the server has to answer a pong message the the connection
//becomes activated. if not, after another ws_ping_period nothing is done, we have a problem
func (wss *wsstream) startPingTicker(c *Client) error {
	//start ping/pong timer
	if wss.ticker != nil {
		return fmt.Errorf("start_ping_ticker called but ticker not nil - will not ping/pong the wstream")
	}

	wss.ticker = time.NewTicker(time.Duration(wsPingPeriod) * time.Second)
	go func() {
		for t := range wss.ticker.C {
			//skip tickers raised during our sleeping
			if t.Before(time.Now().Add(-50 * time.Millisecond)) {
				continue
			}

			if atomic.LoadUint32(&wss.isThereNewMessage) == 1 {
				atomic.StoreUint32(&wss.isThereNewMessage, 0)
			} else { //send ping, the server has to send a pong else something is wrong
				wss.stream.WriteMessage(websocket.TextMessage, []byte("ping"))
				time.Sleep(time.Duration(wsPingPeriod) * time.Second) //wait some more

				if atomic.LoadUint32(&wss.isThereNewMessage) == 0 { //didn't receive the pong
					c.hasClosed <- fmt.Sprintf("detected a connection drop (no ping/pong), forcing stream `%v` close, forcing system exit", wss.subscribedStreams)
				}
			}
		}
	}()

	return nil
}

//handle pong value, returns true iff we have a pong frame
func (wss *wsstream) isPongMessage(m []byte) bool {
	atomic.StoreUint32(&wss.isThereNewMessage, 1)
	return string(m) == "pong"
}

//Close a WSStream and associated ticker
func (wss *wsstream) Close() {
	wss.hasFinished <- struct{}{}

	if wss.stream != nil {
		wss.stream.Close()
	}

	if wss.ticker != nil {
		wss.ticker.Stop()
	}
}

//authenticate a WSStream
func wsStreamAuthenticate(publicKey, privateKey string) string {
	expires := generateExpires()
	signature := computeSignature(privateKey, "GET", "/realtime", expires, "")
	return "{\"op\":\"authKeyExpires\",\"args\":[\"" + publicKey + "\"," + expires + ",\"" + signature + "\"]}"
}

//Client order initialization routine
func (c *Client) newOrderStream(chanSize int) (*wsstream, error) {
	wss, err := newStream(c.logger, c.apiURL, chanSize, "")
	if err != nil {
		return nil, fmt.Errorf("could not open WS stream: %v", err)
	}

	if c.key == nil {
		return nil, fmt.Errorf("no key found, provide credentials before starting streaming")
	}

	if err = wss.authenticate(c.key.public, c.key.private); err != nil {
		return nil, fmt.Errorf("error while authenticating: %v", err)
	}

	if err := wss.subscribe("\"order\""); err != nil {
		return nil, fmt.Errorf("error while subscribing: %v", err)
	}

	c.Orders = make(chan []Order, chanSize)
	atomic.StoreUint64(&wss.subscribedStreams, atomic.LoadUint64(&wss.subscribedStreams)+uint64(SetOrder))
	return wss, nil
}

func (c *Client) restartStreamOnError(wss *wsstream, err error, streamName string) error {
	if strings.Contains(err.Error(), "close 1006 (abnormal closure): unexpected EOF") {
		c.logger.Printf("received error '%v' on %s, forcing system exit...", err, streamName)
		os.Exit(-1)

		s := StreamID(atomic.LoadUint64(&wss.subscribedStreams))
		chanSize := wss.chanSize
		symbol := wss.symbol

		if !c.removeStream(wss) {
			return fmt.Errorf("error while restarting the stream: could not delete old (wrecked) stream from the client")
		}
		wss.Close()

		c.StartStreaming(s, chanSize, symbol)
		return nil
	}

	return fmt.Errorf("could not determine error cause: %v", err)
}

//Client orders messages handling routine
func (c *Client) fillOrdersStream(wss *wsstream) {
	defer func() {
		c.logger.Printf("exiting order wstream reader loop")
		c.removeStream(wss) //streams can be removed by server connection drop
		c.streamsWait.Done()
	}()

	if wss == nil {
		panic("tried to read orders on nil stream")
	}

	c.logger.Printf("starting order wstream reader loop")
	for {
		_, msg, err := wss.stream.ReadMessage()
		if err != nil {
			select {
			case reason := <-c.hasClosed:
				if reason != "" { // if it's a message for the client, ignore it
					c.hasClosed <- reason
				}

				return // otherwise we should stop
			case <-wss.hasFinished:
				return
			default:
				if err := c.restartStreamOnError(wss, err, "orders"); err != nil {
					c.hasClosed <- fmt.Sprintf("error while reading orders stream: %v", err)
					continue
				}

				return
			}
		}

		if wss.isPongMessage(msg) {
			continue
		}

		var data struct {
			Table  string  `json:"table"`
			Action string  `json:"action"`
			Data   []Order `json:"data"`
		}
		if err := json.NewDecoder(bytes.NewReader(msg)).Decode(&data); err != nil {
			c.logger.Printf("could not decode order message: %v, skipping", err)
			continue
		}

		if data.Action == "partial" {
			continue
		}

		if data.Data == nil {
			continue
		}

		select {
		case reason := <-c.hasClosed:
			if reason != "" { // if it's a message for the client, ignore it
				c.hasClosed <- reason
			}

			return
		case c.Orders <- data.Data:
		default:
			c.logger.Printf("client Orders channel is blocking, exiting")
			return
		}
	}
}

//subscribe a stream for OB/Trades
func (c *Client) subscribeOBTStream(chanSize int, s StreamID, wss *wsstream) error {

	//try to authenticate if possible now
	if c.key != nil && !wss.isAuthenticated {
		if err := wss.authenticate(c.key.public, c.key.private); err != nil {
			return fmt.Errorf("could not authenticate: %v", err)
		}
	}

	subscribeArgs := ""
	if s&SetTrade != 0 {
		if atomic.LoadUint64(&wss.subscribedStreams)&uint64(SetTrade) != 0 {
			return fmt.Errorf("trade already subscribed to")
		}
		subscribeArgs += "\"trade:" + wss.symbol + "\""
	}

	if s&SetOrderbookL2 != 0 {
		if c.OrderBook == nil || c.OrderBook[wss.symbol] == nil {
			return fmt.Errorf("requested OB data structure not initialized")
		}

		if c.isOBEmpty == nil {
			c.isOBEmpty = make(map[string]bool)
		}
		c.isOBEmpty[wss.symbol] = true

		if c.OBWaitGroup == nil {
			c.OBWaitGroup = make(map[string]*sync.WaitGroup)
		}
		c.OBWaitGroup[wss.symbol] = &sync.WaitGroup{}
		c.OBWaitGroup[wss.symbol].Add(1)

		if atomic.LoadUint64(&wss.subscribedStreams)&uint64(SetOrderbookL2) != 0 {
			return fmt.Errorf("orderbook already subscribed to")
		}

		if subscribeArgs != "" {
			subscribeArgs += ","
		}

		subscribeArgs += "\"orderBookL2:" + wss.symbol + "\""
	}

	if err := wss.subscribe(subscribeArgs); err != nil {
		return fmt.Errorf("error while subscribing to %s: %v", subscribeArgs, err)
	}

	if s&SetTrade != 0 {
		if c.Trades == nil {
			c.Trades = make(map[string]chan []Trade)
		}

		c.Trades[wss.symbol] = make(chan []Trade, chanSize)
	}

	atomic.StoreUint64(&wss.subscribedStreams, atomic.LoadUint64(&wss.subscribedStreams)+uint64(s))
	return nil
}

//decode and handle a received trade data, returns true if should exit
func (c *Client) handleTradeData(wss *wsstream, msg []byte) (bool, error) {
	var data struct {
		Table  string  `json:"table"`
		Action string  `json:"action"`
		Data   []Trade `json:"data"`
	}

	if err := json.NewDecoder(bytes.NewReader(msg)).Decode(&data); err != nil {
		return false, fmt.Errorf("could not decode trade message: %v", err)
	}

	if data.Action == "partial" {
		return false, nil
	}

	if data.Table != "trade" {
		panic("thought we received a trade message but got a a different one instead.. please contact library author")
	}

	//trouble if this happens (API probably modified)
	if data.Action != "insert" {
		panic("received a non insert trade action, case unseen, please contact library author")
	}

	if data.Data[0].Symbol != wss.symbol {
		panic("received a trade symbol for which the wsstream was not registered, there is a bug somewhere")
	}

	select {
	case c.Trades[wss.symbol] <- data.Data:
		return false, nil
	case reason := <-c.hasClosed:
		if reason != "" { // if it's a message for the client, ignore it
			c.hasClosed <- reason
		}

		return true, nil
	default:
		return true, fmt.Errorf("client Trade channel is blocking, exiting")
	}
}

//handle the insert part of the orderbook update
func (c *Client) handleOBUpdateInsert(wss *wsstream, data []OBUpdate) error {
	for _, w := range data {
		if !c.OrderBook[wss.symbol].BitmexInsert(w.Side == "Sell", w.ID, w.Size, w.Price) {
			c.logger.Printf("bitmex insert action failed [D=`%v`], trying delete and reinsert...", w)
			if !c.OrderBook[wss.symbol].BitmexDelete(w.Side == "Sell", w.ID) {
				return fmt.Errorf("bitmex insert AND delete action failed [D=`%v`], something is really odd", w)
			}
			if !c.OrderBook[wss.symbol].BitmexInsert(w.Side == "Sell", w.ID, w.Size, w.Price) {
				return fmt.Errorf("bitmex insert action failed twice [D=`%v`], something is really odd", w)
			}
		}
	}

	return nil
}

//decode and handle a received orderbook data
func (c *Client) handleOBUpdateData(wss *wsstream, msg []byte) (bool, error) {
	var data struct {
		Table  string     `json:"table"`
		Action string     `json:"action"`
		Data   []OBUpdate `json:"data"`
	}

	if err := json.NewDecoder(bytes.NewReader(msg)).Decode(&data); err != nil {
		return false, fmt.Errorf("could not decode OB message: %v", err)
	}

	if data.Action == "partial" {
		if err := c.OrderBook[wss.symbol].BitmexFill(data.Data); err != nil {
			return true, fmt.Errorf("could not fill initial data: %v", err)
		}

		c.logger.Printf("%s OB stream initialized", wss.symbol)
		c.isOBEmpty[wss.symbol] = false
		c.OBWaitGroup[wss.symbol].Done()
		return false, nil
	}

	//do not process order if OB is still empty (cf docs)
	if c.isOBEmpty[wss.symbol] {
		return false, nil
	}

	if data.Action == "insert" {
		if err := c.handleOBUpdateInsert(wss, data.Data); err != nil {
			return false, err
		}
	} else if data.Action == "update" {
		for _, w := range data.Data {
			if !c.OrderBook[wss.symbol].BitmexUpdate(w.Side == "Sell", w.ID, w.Size) {
				return false, fmt.Errorf("bitmex update action failed [D=`%v`]", w)
			}
		}
	} else if data.Action == "delete" {
		for _, w := range data.Data {
			if !c.OrderBook[wss.symbol].BitmexDelete(w.Side == "Sell", w.ID) {
				return false, fmt.Errorf("bitmex delete action failed [D=`%v`]", w)
			}
		}
	} else {
		return false, fmt.Errorf("unrecognized action: %v", data.Action)
	}

	return false, nil
}

//Client trade+orderbook messages handling routine
func (c *Client) fillOBTStream(wss *wsstream) {
	defer func() {
		c.logger.Printf("exiting obtrade %s wstream reader loop", wss.symbol)
		c.removeStream(wss) //streams can be removed by server connection drop
		c.streamsWait.Done()
	}()

	if wss == nil {
		panic("tried to read obtrades on nil stream")
	}

	c.logger.Printf("starting obtrade %s wstream reader loop", wss.symbol)
	for {
		_, msg, err := wss.stream.ReadMessage()
		if err != nil {
			select {
			case reason := <-c.hasClosed:
				if reason != "" { // if it's a message for the client, ignore it
					c.hasClosed <- reason
				}

				return
			case <-wss.hasFinished:
				return
			default:
				if err := c.restartStreamOnError(wss, err, "OB+Trade"); err != nil {
					c.hasClosed <- fmt.Sprintf("error while reading OB+Trade stream: %v", err)
					continue
				}

				return
			}
		}

		if wss.isPongMessage(msg) {
			continue
		}

		//quickly test message
		var shouldExit bool
		msgS := string(msg)
		if strings.HasPrefix(msgS, `{"table":"trade`) {
			if shouldExit, err = c.handleTradeData(wss, msg); err != nil {
				c.logger.Printf("trade update error: %v", err)
				if shouldExit {
					return
				}

				continue
			}
		} else if strings.HasPrefix(msgS, `{"table":"orderBookL2`) {
			if shouldExit, err = c.handleOBUpdateData(wss, msg); err != nil {
				c.logger.Printf("orderbook update error: %v", err)
				if shouldExit {
					return
				}

				continue
			}
		} else {
			c.logger.Printf("unrecognized table type [%s], please contact library author", msgS)
		}

		if shouldExit {
			return
		}
	}
}
