//This file is part of gobitmex
//
//
//gobitmex is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation version 3 of the License.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//
// Copyright (C) 2018 Pierre NN

package gobitmex

import (
	"io/ioutil"
	"log"
	"net/http"
	"testing"
	"time"
)

var testIsTestNet = false

//TestSimpleWssStream: test we can open a simple wss stream
func TestSimpleWssStream(t *testing.T) {
	ws, err := newStream(log.New(ioutil.Discard, "", 0), "www.bitmex.com", 0, "")
	if err != nil {
		t.Errorf("Stream creation failed %v", err)
	}

	ws.Close()
}

//TestOrderStream: test authenticated WS stream endpoint order
//see main_test.go top comment for testing authenticated endpoints
func TestOrderStream(t *testing.T) {
	client, err := NewClient(testIsTestNet)
	if err != nil {
		t.Errorf("NewClient returned an error: %v", err)
	}

	if !testing.Verbose() {
		client.SetLogger(log.New(ioutil.Discard, "", 0))
	}

	//user_id is just used to test answer from the server
	publicKey, privateKey, userID := getMyKeys()
	err = client.Login(publicKey, privateKey)
	if err != nil {
		t.Errorf("Login returned an error: %v", err)
	}

	//skip test is user_id is 0 (no api key provided)
	if userID == 0 {
		t.Skip("skipping test as no api key is defined")
	}

	if err := client.StartStreaming(SetOrder, 10, ""); err != nil {
		t.Errorf("Could not start order streaming: %v", err)
	}

	go func() {
		for {
			d := <-client.Orders
			if d == nil {
				client.logger.Printf("channel closed")
				return
			}

			client.logger.Printf("Received order: %v", d)
		}
	}()

	//wait for the ping ticker to try a ping/pong
	time.Sleep(2 * time.Second)
	client.Close()
}

//TestSingleOBTrade: test streaming of one single OBTrade instance
func TestSingleTrade(t *testing.T) {
	client, err := NewClient(true)
	if err != nil {
		t.Errorf("NewClient returned an error: %v", err)
	}

	if !testing.Verbose() {
		client.SetLogger(log.New(ioutil.Discard, "", 0))
	}

	if err := client.StartStreaming(SetTrade, 10, "XBTUSD"); err != nil {
		t.Errorf("Could not start Trade streaming: %v", err)
	}

	go func() {
		for {
			d := <-client.Trades["XBTUSD"]
			if d == nil {
				client.logger.Printf("channel closed")
				return
			}

			client.logger.Printf("Received trade: %v", d)
		}
	}()

	//give some time to receive some trades
	time.Sleep(10 * time.Second)
	client.Close()
}

//TestSingleOB: test streaming of one single OBTrade instance
func TestSingleOB(t *testing.T) {
	client, err := NewClient(testIsTestNet)
	if err != nil {
		t.Errorf("NewClient returned an error: %v", err)
	}

	if !testing.Verbose() {
		client.SetLogger(log.New(ioutil.Discard, "", 0))
	}

	client.OrderBook = make(map[string]OrderBookHandler)
	xbtOB := SimpleOB{}
	client.OrderBook["XBTUSD"] = &xbtOB

	if err := client.StartStreaming(SetOrderbookL2, 10, "XBTUSD"); err != nil {
		t.Fatalf("Could not start OB streaming: %v", err)
	}

	client.OBWaitGroup["XBTUSD"].Wait()

	xbtOB.Mu[0].Lock()
	xbtOB.Mu[1].Lock()
	buytop := xbtOB.Data[0][len(xbtOB.Data[0])-1]
	selltop := xbtOB.Data[1][len(xbtOB.Data[1])-1]
	client.logger.Printf("Top OB prices: BUY[%v @ %v] SELL[%v @ %v]\n", buytop.Size, buytop.Price, selltop.Size, selltop.Price)
	xbtOB.Mu[0].Unlock()
	xbtOB.Mu[1].Unlock()
	time.Sleep(2 * time.Second)
	client.Close()
}

//TestOrderAlert: test if we properly trigger order alerts
func TestOrderAlert(t *testing.T) {
	client, err := NewClient(true)
	if err != nil {
		t.Errorf("NewClient returned an error: %v", err)
	}

	if !testing.Verbose() {
		client.SetLogger(log.New(ioutil.Discard, "", 0))
	}

	t.Skipf("This test created an order, so it is skipped by default - remove this line to try")

	//user_id is just used to test answer from the server
	publicKey, privateKey, userID := getMyKeys()
	err = client.Login(publicKey, privateKey)
	if err != nil {
		t.Errorf("Login returned an error: %v", err)
	}

	//skip test is user_id is 0 (no api key provided)
	if userID == 0 {
		t.Skip("skipping test as no api key is defined")
	}

	client.GetLogger().SetFlags(client.GetLogger().Flags() | log.Lmicroseconds)
	client.OrderBook = make(map[string]OrderBookHandler)
	xbtOB := SimpleOB{}
	client.OrderBook["XBTUSD"] = &xbtOB

	if err := client.StartStreaming(SetTrade|SetOrderbookL2, 100, "XBTUSD"); err != nil {
		t.Fatalf("Could not start order streaming: %v", err)
	}

	//quickly wait for the streaming to start
	time.Sleep(10 * time.Second)

	//make a channel to receive price updates
	priceDiffs := make(chan int64, 10)

	//our specification of new order data
	xbtOB.Mu[1].Lock()
	wantedPrice := xbtOB.Data[1][len(xbtOB.Data[1])-1].Price
	xbtOB.Mu[1].Unlock()
	wantedSize := uint32(1234)
	wantedSide := "Sell"
	wantedOppositeSide := "Buy"
	xbtOB.SetAlert(true, wantedPrice, wantedSize, priceDiffs)

	go func() {
		hasReceivedUpdate := false

		var contractsBefore int64
		var contractsAfter int64
		var amntTraded int64

		for {
			select {
			case trades := <-client.Trades["XBTUSD"]:
				if trades == nil {
					client.logger.Printf("trade channel closed")
					return
				}

				for _, t := range trades {
					//see if this matches our order level
					if t.Side == wantedOppositeSide && t.Price == wantedPrice {
						amntTraded += int64(t.Size)
					}
				}

				client.logger.Printf("Received a trade update: %v, new amnt traded=%d", trades, amntTraded)

			case update := <-priceDiffs:
				if update == 0 {
					client.logger.Printf("update channel closed")
					return
				}

				client.logger.Printf("Received a price diff update: %v", update)

				if !hasReceivedUpdate { //the first data is the amount of contracts before our alert
					contractsBefore = update
					hasReceivedUpdate = true
				} else {
					if update > 0 { //if new contracts are created, they are created after us
						contractsAfter += update
					} else {
						shownTradedAmnt := -1 * update

						//if more contracts are shown as traded than the actual amount of traded contracts,
						if shownTradedAmnt >= amntTraded {
							//then the rest of the contracts might be after our position
							contractsBefore -= amntTraded
						} else if shownTradedAmnt < amntTraded { //else there is hidden contracts of amount amntTraded-shownTradedAmnt
							contractsBefore -= shownTradedAmnt
						}
					}
				}
				client.logger.Printf("Position: %d  - our order - %d", contractsBefore, contractsAfter)
				amntTraded = 0
			}
		}
	}()

	req, err := client.NewRequest(http.MethodPost, "/order", map[string]interface{}{"symbol": "XBTUSD", "side": wantedSide, "orderQty": wantedSize, "price": wantedPrice})
	if err != nil {
		t.Fatalf("new request fail: %v", err)
	}

	rep, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatalf("send request fail: %v", err)
	}

	body, err := ioutil.ReadAll(rep.Body)
	if err != nil {
		t.Errorf("could not read HTTP response body: %v", err)
	}
	bodyString := string(body)
	rep.Body.Close()

	client.logger.Printf("HTTP=%s", bodyString)
	time.Sleep(30 * time.Second)
	client.Close()

}
