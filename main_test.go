//This file is part of gobitmex
//
//
//gobitmex is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation version 3 of the License.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//
// Copyright (C) 2018 Pierre NN

package gobitmex

import (
	"encoding/json"
	"net/http"
	"testing"
)

/***
* Note concerning testing for authenticated endpoints:
*
* a function `Get_my_keys`  is defined in an other file in the same package
* (not included in git history), define this as your api keys for this test to be used
* function name needs to be exported for privacy (git history)
* e.g.:
* func Get_my_keys() (string, string, int) {
* 	return "" (public key), "" (private key), 0 (user id returned by bitmex api for test)
* }
***/

//Request unit test (non auth version):
//	check that the version answered by bitmex api matches ours
func TestHTTPNonAuth(t *testing.T) {
	c, err := NewClient(testIsTestNet)
	if err != nil {
		t.Errorf("NewClient returned an error: %v", err)
	}

	r, err := c.NewRequest("GET", "/", nil)
	if err != nil {
		t.Errorf("Request returned an error: %v", err)
	}

	s, err := http.DefaultClient.Do(r)
	if err != nil {
		t.Errorf("Request execution returned an error: %v", err)
	}
	var data struct {
		Version string `json:"version"`
	}

	if err := json.NewDecoder(s.Body).Decode(&data); err != nil {
		t.Fatalf("could not decode json: %v", err)
	}

	if data.Version != "1.2.0" {
		t.Logf("WARNING: the library has been tested for api version 1.2.0, please contact the author to report a new version is available")
	}
}

//Request unit test (auth version):
// test if the keys supplied match the id in bitmex database
func TestHTTPAuth(t *testing.T) {
	c, err := NewClient(testIsTestNet)
	if err != nil {
		t.Errorf("NewClient returned an error: %v", err)
	}

	//user_id is just used to test answer from the server
	publicKey, privateKey, userID := getMyKeys()
	err = c.Login(publicKey, privateKey)
	if err != nil {
		t.Errorf("Login returned an error: %v", err)
	}

	if c.key.public != publicKey || c.key.private != privateKey {
		t.Errorf("Login: keys are not set")
	}

	//skip test is user_id is 0 (no api key provided)
	if userID == 0 {
		t.Skip("skipping test as no api key is defined")
	}

	//you can try modifying leverage e.g.
	//r, err := c.NewRequest("POST", "/position/leverage", map[string]interface{}{"symbol": "ADAZ18", "leverage": 5})
	r, err := c.NewRequest("GET", "/user", nil)
	if err != nil {
		t.Errorf("Request to private api failed:%v", err)
	}

	s, err := http.DefaultClient.Do(r)
	if err != nil {
		t.Errorf("Request execution returned an error: %v", err)
	}

	var data struct {
		ID    int       `json:"id,omitempty"`
		Error HTTPError `json:"error,omitempty"`
	}

	if err := json.NewDecoder(s.Body).Decode(&data); err != nil {
		t.Fatalf("could not decode json: %v", err)
	}

	if data.Error != (HTTPError{}) {
		t.Fatalf("HTTP Api returned an error: %v", data.Error)
	}

	if userID != data.ID {
		t.Errorf("Bitmex api returned wrong user id: %d, should be %d", data.ID, userID)
	}

}
