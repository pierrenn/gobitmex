//This file is part of gobitmex
//
//
//gobitmex is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation version 3 of the License.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//
// Copyright (C) 2018 Pierre NN

package gobitmex

import (
	"bufio"
	"bytes"
	"encoding/json"
	"io/ioutil"
	"os"
	"reflect"
	"testing"
)

//Test our simple order book search functions
func TestSimpleOBSearch(t *testing.T) {
	var tests = []int{1, 3, 4, 5, 8, 13, 42, 43, 45}
	for _, v := range tests {
		ob := SimpleOB{}
		maxID := v
		for i := 1; i < maxID; i++ {
			ob.Data[0] = append(ob.Data[0], PriceLevel{uint64(i), uint32(i * 100), float32(i * 10)})
		}

		for i := maxID - 1; i > 0; i-- {
			ob.Data[1] = append(ob.Data[1], PriceLevel{uint64(i), uint32(i * 100), float32(i * 10)})
		}

		for i := 1; i < maxID; i++ {
			//find an index on buys (for simplicity)
			a, b := ob.findIndex(0, uint64(i))
			if !b || a != (i-1) {
				t.Fatalf("error while finding index (MAX=%v): %v -> %v (%v) [D=%v]", v, i, a, b, ob.Data[0])
			}

			//find a matching price
			arg := float32(i * 10)
			a, b = ob.FindPrice(false, arg)
			if !b {
				t.Fatalf("error while finding existing buy price (MAX=%v): %v -> %v (%v) (i=%v) [D=%v]", v, arg, a, b, i, ob.Data[0])
			}

			a, b = ob.FindPrice(true, arg)
			if !b {
				t.Fatalf("error while finding existing sell price (MAX=%v): %v -> %v (%v) (i=%v) [D=%v]", v, arg, a, b, i, ob.Data[1])
			}

			//find a non matching price
			var nonMatchTests = []int{1, 2, 3, 4, 5, 6, 7, 8, 9}
			for _, k := range nonMatchTests {
				arg := float32((i * 10) + k)
				a, b = ob.FindPrice(false, arg)
				if b || a != i {
					t.Fatalf("error while finding non existing buy price (MAX=%v): %v -> %v (%v) (i=%v) [D=%v]", v, arg, a, b, i, ob.Data[0])
				}

				a, b = ob.FindPrice(true, arg)
				if b || a != maxID-i-1 {
					t.Fatalf("error while finding non existing sell price (MAX=%v): %v -> %v (%v) (i=%v) [D=%v]", v, arg, a, b, i, ob.Data[1])
				}
			}
		}
	}
}

//Test our simple order book CRUD api
func TestSimpleOBCRUD(t *testing.T) {
	makepl := func(i uint64) PriceLevel { return PriceLevel{i, uint32(i * 100), float32(i * 10)} }
	var tests = []struct {
		input []int

		insertOutputBuy []PriceLevel
		deleteOutputBuy []PriceLevel

		insertOutputSell []PriceLevel
		deleteOutputSell []PriceLevel
	}{
		{[]int{4, 56, 7, 14, 67, 45, 22, 42},
			[]PriceLevel{makepl(4), makepl(7), makepl(14), makepl(22), makepl(42), makepl(45), makepl(56), makepl(67)},
			[]PriceLevel{makepl(7), makepl(14), makepl(42), makepl(45), makepl(56)},
			[]PriceLevel{makepl(67), makepl(56), makepl(45), makepl(42), makepl(22), makepl(14), makepl(7), makepl(4)},
			[]PriceLevel{makepl(56), makepl(45), makepl(42), makepl(14), makepl(7)},
		},
	}

	for _, myTest := range tests {
		ob := SimpleOB{}

		for _, v := range myTest.input {
			pl := makepl(uint64(v))
			if !ob.BitmexInsert(false, pl.ID, pl.Size, pl.Price) || !ob.BitmexInsert(true, pl.ID, pl.Size, pl.Price) {
				t.Fatalf("error while constructing order books")
			}
		}

		if !reflect.DeepEqual(ob.Data[0], myTest.insertOutputBuy) {
			t.Fatalf("could not properly insert buy data, got: %v", ob.Data[0])
		}

		if !reflect.DeepEqual(ob.Data[1], myTest.insertOutputSell) {
			t.Fatalf("could not properly insert sell data, got: %v", ob.Data[1])
		}

		ob.BitmexDelete(false, 4)
		ob.BitmexDelete(false, 22)
		ob.BitmexDelete(false, 67)
		if !reflect.DeepEqual(ob.Data[0], myTest.deleteOutputBuy) {
			t.Fatalf("could not properly insert buy data, got: %v", ob.Data[0])
		}

		ob.BitmexDelete(true, 4)
		ob.BitmexDelete(true, 22)
		ob.BitmexDelete(true, 67)
		if !reflect.DeepEqual(ob.Data[1], myTest.deleteOutputSell) {
			t.Fatalf("could not properly insert buy data, got: %v", ob.Data[1])
		}

		ob.BitmexUpdate(false, 14, 1)
		ob.BitmexUpdate(true, 14, 1)
		buyidx, _ := ob.findIndex(0, 14)
		sellidx, _ := ob.findIndex(1, 14)
		if ob.Data[0][buyidx].Size != 1 || ob.Data[1][sellidx].Size != 1 {
			t.Fatalf("bitmex update failed")
		}

		//TODO async BitmexUpdate test
	}
}

//Benchmark BitmexFill vs BitmexInsert for an OB representation
//reads a file partial.json (got from the server)
//note: this is not representative of the workload were going to send to BitmexInsert !
func BenchmarkOBFill(b *testing.B) {
	msg, err := ioutil.ReadFile("./benchfiles/1/partial.json")
	if err != nil {
		b.Skipf("could not read the testfiles/partial.json benchmark input")
	}

	var data struct {
		Table  string     `json:"table"`
		Action string     `json:"action"`
		Data   []OBUpdate `json:"data"`
	}

	if err := json.NewDecoder(bytes.NewReader(msg)).Decode(&data); err != nil {
		b.Skipf("could not decode testfile message: %v", err)
	}

	var funcs = []struct {
		name string
		f    func([]OBUpdate)
	}{
		{"BitmexFill",
			func(s []OBUpdate) {
				ob := SimpleOB{}
				ob.BitmexFill(s)
			},
		}, {"BitmexInsert",
			func(s []OBUpdate) {
				ob := SimpleOB{}
				for _, v := range data.Data {
					ob.BitmexInsert(v.Side == "Sell", v.ID, v.Size, v.Price)
				}
			},
		},
	}

	for _, f := range funcs {
		b.Run(f.name, func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				f.f(data.Data)
			}
		})
	}
}

//Test what is faster to use for OB, add your own OB structure here
//reads file partial.json/updates.json (got from the server)
//from some testings:
// baseline = dummy orderbook (standard array ordering, no striding)
// ~20% improvement using regular order OB and striding of 4
// ~10% improvement ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 8
func BenchmarkOBUpdates(b *testing.B) {
	msg, err := ioutil.ReadFile("./benchfiles/1/partial.json")
	if err != nil {
		b.Skipf("could not read the benchfiles/partial.json benchmark input")
	}

	var initData struct {
		Table  string     `json:"table"`
		Action string     `json:"action"`
		Data   []OBUpdate `json:"data"`
	}

	if err := json.NewDecoder(bytes.NewReader(msg)).Decode(&initData); err != nil {
		b.Skipf("could not decode testfile message: %v", err)
	}

	updateFile, err := os.Open("./benchfiles/1/update_list.json")
	if err != nil {
		b.Skipf("could not open json update file")
	}

	var updateData []struct {
		Table  string     `json:"table"`
		Action string     `json:"action"`
		Data   []OBUpdate `json:"data"`
	}

	if err := json.NewDecoder(bufio.NewReader(updateFile)).Decode(&updateData); err != nil {
		b.Skipf("could not decode testfile message: %v", err)
	}

	//make your OBs:
	var obTypes = []struct {
		name string
		ob   OrderBookHandler
	}{
		{"basic", &SimpleOB{}},
	}

	for _, ob := range obTypes {
		ob.ob.BitmexFill(initData.Data)
		b.Run(ob.name, func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				for _, v := range updateData {
					if v.Action == "insert" {
						for _, w := range v.Data {
							ob.ob.BitmexInsert(w.Side == "Sell", w.ID, w.Size, w.Price)
						}
					} else if v.Action == "update" {
						for _, w := range v.Data {
							ob.ob.BitmexUpdate(w.Side == "Sell", w.ID, w.Size)
						}
					} else if v.Action == "delete" {
						for _, w := range v.Data {
							ob.ob.BitmexDelete(w.Side == "Sell", w.ID)
						}
					} else {
						b.Fatalf("unrecognized action: %v", v)
					}
				}
			}
		})
	}
}
