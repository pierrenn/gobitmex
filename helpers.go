//This file is part of gobitmex
//
//
//gobitmex is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation version 3 of the License.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//
// Copyright (C) 2018 Pierre NN

package gobitmex

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"strconv"
	"sync"
	"time"
)

/**** AUTH HELPERS ***/
/*********************/
//the server will ignore requests if they take more than 5 sec (see api-expires parameter in API doc)
const apiTimingExpirySec = 5

var signatureTimerLock sync.Mutex

//generate an api-expires token
func generateExpires() string {
	return strconv.FormatInt(time.Now().Unix()+apiTimingExpirySec, 10)
}

//compute aithenticated endpoints signature
func computeSignature(secret, method, path, expires, body string) string {
	str := method + path + expires + body
	sig := hmac.New(sha256.New, []byte(secret))
	sig.Write([]byte(str))
	return hex.EncodeToString(sig.Sum(nil))
}

/***** HIDDEN ORDERS HELPERS ****/
/*******************************/
func isSellToInt(isSell bool) int {
	if isSell {
		return 1
	}

	return 0
}
