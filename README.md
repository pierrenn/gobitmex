# gobitmex

[![GoReport](https://goreportcard.com/badge/gitlab.com/pierrenn/gobitmex)](https://goreportcard.com/report/gitlab.com/pierrenn/gobitmex)
[![Build Status](https://gitlab.com/pierrenn/gobitmex/badges/master/pipeline.svg)](https://gitlab.com/pierrenn/gobitmex/pipelines)
[![CodeCov](https://codecov.io/gl/pierrenn/gobitmex/branch/master/graph/badge.svg)](https://codecov.io/gl/pierrenn/gobitmex)
[![GoDoc](https://godoc.org/gitlab.com/pierrenn/gobitmex?status.svg)](https://godoc.org/gitlab.com/pierrenn/gobitmex)

gobitmex is a tiny library to use the [bitmex](https://www.bitmex.com) API in your go program:
  - construct order book from L2 to get quick data from bitmex
  - use your own orderbook data structure (see [`OrderBookHandler`](https://godoc.org/gitlab.com/pierrenn/gobitmex#OrderBookHandler) interface) or use the basic one provided as an example
  - get updates of a price level queue after a specific update has been seen (helps you to detect hidden contracts/your order position in the OB queue/...)
  - includes a very simple (not swagger bloated) HTTP API so you can use REST authenticated endpoints with timestamps compatible with the WS authenticated endpoints

# Getting a new client and authentication

Created a new client and register new API Keys as simply as:
```go
should_use_testnet := false
bitmexClient := gobitmex.NewClient(should_use_testnet)
bitmexClient.Login(publicApiLey, privateApiKey)
defer bitmex_client.Close()
```

Calling `Login` register your public and private keys and use authentication for all API calls.
Use 2 clients if you want to mix authenticated/not authenticated API calls.

Don't forget to close the client (``bitmexClient.Close()``)!

# WS Api

For now, only `orderBookL2` (realtime orderbook), `trade` (recent trades), `order` (user's new orders/fills/cancel/...) are supported.
If you need something else feel free to do a feature/pull request. A unique websocket stream is created for `orderBookL2`
and `trade` subscriptions (one stream for each symbol). `order` always use a unique (different) websocket stream.



You can subscribe to stream individually, or multiple streams at the same time using the `StartStreaming` function.
For `trade` and `order` streams, the library uses buffered channels (respectively `Client.Trade[symbol_name]` and `Client.Orders`) to send data to the user.


The first argument of `StartStreaming` is the type of stream (see [`gobitmex.StreamID`](https://godoc.org/gitlab.com/pierrenn/gobitmex#StreamId)), the second argument is the size of the buffer (if needed),
the third is the currency symbol for the stream (if needed).

Used channel are closed when the connection is closed (by the user, by the server, or by bad network).
We never send you `nil` data, so a `nil` received means that the connection has been closed (just re-run `StartStreaming` to reopen it).
If a channel is full (blocking), we will disconnect from the stream and close the channel.


## Get account's orders

```go
//client is an authenticated gobitmex.Client
//get all the new orders for the account
if err := client.StartStreaming(gobitmex.SetOrder, 10, ""); err != nil {
	return fmt.Errorf("Could not start order streaming: %v", err)
}

go func() {
	for {
		d := <-client.Orders
		if d == nil {
			client.logger.Printf("channel closed")
			return
		}

		client.logger.Printf("Received order: %v", d)
	}
}()
```

## Get OrderBook and Trade data

```go
client.OrderBook = make(map[string]OrderBookHandler)
xbtOB := SimpleOB{}
client.OrderBook["XBTUSD"] = &xbtOB

if err := client.StartStreaming(gobitmex.SetOrderbookL2 | gobitmex.SetTrade, 10, "XBTUSD"); err != nil {
	return fmt.Errorf("Could not start OB streaming: %v", err)
}

go func() {
	for {
		d := <-client.Trades["XBTUSD"]
		if d == nil {
			client.logger.Printf("channel closed")
			return
		}

        client.logger.Printf("Received trade(s): %v", v)
	}
}()

for {
    xbtOB.Mu[0].RLock()
    buyTop := xbtOB.Data[0][len(xbtOB.Data[0])-1]
    xbtOB.Mu[0].RUnlock()

    xbtOB.Mu[1].RLock()
    sellTop := xbtOB.Data[1][len(xbtOB.Data[1])-1]
    xbtOB.Mu[1].RUnlock()

    client.logger.Printf("(buy:%d/%d,sell:%d/%d)", buyTop.Price, atomic.LoadUint32(&buyTop.Size), sellTop.Price, atomic.LoadUint32(&sellTop.Size))

    time.Sleep(1 * time.Second)
}
```


The above code alerts every time a trade matches an hidden order and prints the top of the buy/sell order book every 1 second (for `XBTUSD`).
This will fill `xbtOB` with a full orderbook.
`xbtOB` is any data structure implementing the `OrderBookHandler` interface. See the  [GoDoc](https://godoc.org/gitlab.com/pierrenn/gobitmex) for more information.


An example of such structure is provided through `SimpleOB`.
In `SimpleOB`, the data is simply registered in 2 price ordered slices, `Data[0]` (buys) and `Data[1]` (sells) where the end of the slices are the
top prices in the order book. Note that you should use a read lock when reading data from it (if you want a lock free data structure, roll your own, this
is not the objective of this library).


`SimpleOB` also provides a simple [FindPrice](https://godoc.org/gitlab.com/pierrenn/gobitmex#SimpleOB.FindPrice) helper to find a price in the OB
(read locking left to user).


## Get OB updates alert

`SimpleOB` also provides you with simple `SetAlert`/`StopAlert` functions to get price updates for a certain order queue.
See [`TestOrderAlert`](ws_test.go#L140) in `ws_test.go` to have an example of how to use `SetAlert` to detect hidden orders and your position in the queue.

# REST Api

`gobitmex` simply provides a function `NewRequest` that computes signatures/proper headers/proper body once the client is aunthenticated.

We also provides a `gobitmex.HTTPError` struct to add in your json response decoding structs. See example below.

Simple usage example to set your leverage to `x50` for the `ADAZ18` market (가즈아아아):

```go
//set an authenticated client bitmex_client to a given leverage
func set_leverage_ada(bitmexClient gobitmex.Client) (float32, error) {
    //Create a new HTTP request
    req, err := bitmexClient.NewRequest(http.MethodPost, "/position/leverage", map[string]interface{}{"symbol":"ADAZ18","leverage":50})
    if err != nil {
        return 0,fmt.Errorf("NewRequest failed: %v", err)
    }

    //Run the request: we simply use http.DefaultClient here but use another one if you prefer
    rep, err := http.DefaultClient.Do(req)
    if err != nil {
        return 0,fmt.Errorf("Request failed: %v", err)
    }

    //Extract only possible errors/returned API leverage
    var data struct {
        Leverage float32 `json:"leverage,omitempty"`
        Error gobitmex.HTTPError `json:"error,omitempty"`
    }

    if err := json.NewDecoder(rep.Body).Decode(&data); err != nil {
        return 0,fmt.Errorf("could not decode json: %v", err)
    }

    if data.Error != (gobitmex.HTTPError{}) {
        return 0,fmt.Errorf("HTTP API returned an error: %v", data.Error)
    }

    //return leverage set by bitmex API
    return data.Leverage, nil
}
```

# Others

 - By default, the library will log data to `os.Stderr`. You can change the logger being used with `SetLogger` (and get it via `GetLogger`). For example, disable library logs using:
```go
client.SetLogger(log.New(ioutil.Discard, "", 0))
```
 - ...

# Dependencies

Only gorilla-websocket (tested with v1.4.2, see `go.mod`)


# Todos
 - Increase code coverage
 - Add other WS streams (useless for the package author at the moment :)
 - Don't hesitate to create an issue!
----

