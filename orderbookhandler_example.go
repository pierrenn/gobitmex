//This file is part of gobitmex
//
//
//gobitmex is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation version 3 of the License.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//
// Copyright (C) 2018 Pierre NN

package gobitmex

import (
	"fmt"
	"sort"
	"sync"
	"sync/atomic"
)

//PriceLevel is of size 128, being optimistic: 1 cache line = 4 price levels, so
//we SHOULD do our loops with a stride of four
//BUT the go optimizer must do some stuff and after testing, a stride of 8 is ~10%
//faster (files provided in ./benchfiles/1/)
const strideSimpleOB = 8

//SimpleOB is a simple data structure implementing OrderBookHandler interface.
//It's really bad, not lock free and so on...
//
//For advanced usage, you should use your own
type SimpleOB struct {
	Mu   [2]sync.RWMutex
	Data [2][]PriceLevel

	outAlert        chan<- int64
	isAlertOn       bool
	priceTest       float32
	sizeTest        uint32
	isSellSideAlert bool
	alertMutex      sync.Mutex
}

//find index matching an id (caller responsibility to lock)
func (book *SimpleOB) findIndex(dataIdx int, id uint64) (int, bool) {
	for i := len(book.Data[dataIdx]) - strideSimpleOB; i >= 0; i = i - strideSimpleOB {
		//hand loop unrolling
		if book.Data[dataIdx][i].ID == id {
			return i, true
		}

		if book.Data[dataIdx][i+1].ID == id {
			return i + 1, true
		}

		if book.Data[dataIdx][i+2].ID == id {
			return i + 2, true
		}

		if book.Data[dataIdx][i+3].ID == id {
			return i + 3, true
		}

		if book.Data[dataIdx][i+4].ID == id {
			return i + 4, true
		}

		if book.Data[dataIdx][i+5].ID == id {
			return i + 5, true
		}

		if book.Data[dataIdx][i+6].ID == id {
			return i + 6, true
		}

		if book.Data[dataIdx][i+7].ID == id {
			return i + 7, true
		}
	}

	leftOver := len(book.Data[dataIdx]) % strideSimpleOB
	for i := 0; i < leftOver; i++ {
		if book.Data[dataIdx][i].ID == id {
			return i, true
		}
	}

	return 0, false
}

//FindPrice finds the index corresponding to a given price.
//It returns the index and whether or not the price is already present in the OB
//
//WARNING: It's the caller responsibility to lock/unlock!
func (book *SimpleOB) FindPrice(isSell bool, price float32) (int, bool) {
	if isSell {
		return book.findPriceSell(price)
	}

	return book.findPriceBuy(price)
}

//Compare function is different for buy/sells
//go generics would be handy here...
func (book *SimpleOB) findPriceBuy(price float32) (int, bool) {
	for i := len(book.Data[0]) - strideSimpleOB; i >= 0; i = i - strideSimpleOB {
		//price is ordered, se we can assume that
		// + hopefully the cache will fill the next 512bits (hopefully)
		if !(price >= book.Data[0][i].Price) {
			continue
		}

		if price >= book.Data[0][i+7].Price {
			return i + 8, book.Data[0][i+7].Price == price
		}

		if price >= book.Data[0][i+6].Price {
			return i + 7, book.Data[0][i+6].Price == price
		}

		if price >= book.Data[0][i+5].Price {
			return i + 6, book.Data[0][i+5].Price == price
		}

		if price >= book.Data[0][i+4].Price {
			return i + 5, book.Data[0][i+4].Price == price
		}

		if price >= book.Data[0][i+3].Price {
			return i + 4, book.Data[0][i+3].Price == price
		}

		if price >= book.Data[0][i+2].Price {
			return i + 3, book.Data[0][i+2].Price == price
		}

		if price >= book.Data[0][i+1].Price {
			return i + 2, book.Data[0][i+1].Price == price
		}

		if price >= book.Data[0][i].Price {
			return i + 1, book.Data[0][i].Price == price
		}
	}

	if len(book.Data[0]) == 0 {
		return 0, false
	}

	leftOver := (len(book.Data[0]) - 1) % strideSimpleOB //left_over can be 0..stride-1
	for i := leftOver; i >= 0; i-- {
		if price >= book.Data[0][i].Price {
			return i + 1, book.Data[0][i].Price == price
		}
	}

	//leftover is 0
	return 0, book.Data[0][0].Price == price
}

func (book *SimpleOB) findPriceSell(price float32) (int, bool) {
	for i := len(book.Data[1]) - strideSimpleOB; i >= 0; i = i - strideSimpleOB {
		//price is ordered, se we can assume that
		// + hopefully the cache will fill the next 512bits (hopefully)
		if !(price <= book.Data[1][i].Price) {
			continue
		}

		if price <= book.Data[1][i+7].Price {
			return i + 8, book.Data[1][i+7].Price == price
		}

		if price <= book.Data[1][i+6].Price {
			return i + 7, book.Data[1][i+6].Price == price
		}

		if price <= book.Data[1][i+5].Price {
			return i + 6, book.Data[1][i+5].Price == price
		}

		if price <= book.Data[1][i+4].Price {
			return i + 5, book.Data[1][i+4].Price == price
		}

		if price <= book.Data[1][i+3].Price {
			return i + 4, book.Data[1][i+3].Price == price
		}

		if price <= book.Data[1][i+2].Price {
			return i + 3, book.Data[1][i+2].Price == price
		}

		if price <= book.Data[1][i+1].Price {
			return i + 2, book.Data[1][i+1].Price == price
		}

		if price <= book.Data[1][i].Price {
			return i + 1, book.Data[1][i].Price == price
		}
	}

	if len(book.Data[1]) == 0 {
		return 0, false
	}

	leftOver := (len(book.Data[1]) - 1) % strideSimpleOB //left_over can be 0..stride-1
	for i := leftOver; i >= 0; i-- {
		if price <= book.Data[1][i].Price {
			return i + 1, book.Data[1][i].Price == price
		}
	}

	//leftover is 0
	return 0, book.Data[1][0].Price == price
}

//BitmexUpdate implements OrderBookHandler
func (book *SimpleOB) BitmexUpdate(isSell bool, id uint64, size uint32) bool {
	dataIdx := isSellToInt(isSell)

	book.Mu[dataIdx].RLock() //we only read the table
	defer book.Mu[dataIdx].RUnlock()

	k, b := book.findIndex(dataIdx, id)
	if !b {
		return false
	}

	book.alertMutex.Lock() //lock almost never active
	if book.isSellSideAlert == isSell && book.Data[dataIdx][k].Price == book.priceTest {
		if size == book.Data[dataIdx][k].Size { //so we don't send 0 to the channel by some unforeseen circonstance
			panic("received an update of size 0?!")
		}

		if book.isAlertOn {
			select {
			case book.outAlert <- int64(size) - int64(book.Data[dataIdx][k].Size):
			default: //if the channel is blocking, just skip the value, we really shouldn't be blocking here
			}
		} else {
			if (size - book.Data[dataIdx][k].Size) == book.sizeTest {
				book.isAlertOn = true
				book.outAlert <- int64(book.Data[dataIdx][k].Size)
			}
		}
	}
	book.alertMutex.Unlock()

	atomic.StoreUint32(&book.Data[dataIdx][k].Size, size)
	return true
}

//SetAlert will fill the out channel with price updates after the price level price has been added the quantity qty
func (book *SimpleOB) SetAlert(isSell bool, price float32, qty uint32, out chan<- int64) {
	book.alertMutex.Lock()
	defer book.alertMutex.Unlock()

	book.isAlertOn = false
	book.isSellSideAlert = isSell
	book.sizeTest = qty
	book.priceTest = price
	book.outAlert = out
}

//StopAlert stops an alert of happening
func (book *SimpleOB) StopAlert() {
	book.alertMutex.Lock()
	defer book.alertMutex.Unlock()

	book.isAlertOn = false
	book.sizeTest = 0
	book.priceTest = 0
	book.outAlert <- 0
}

//BitmexDelete implements OrderBookHandler
func (book *SimpleOB) BitmexDelete(isSell bool, id uint64) bool {
	dataIdx := isSellToInt(isSell)
	book.Mu[dataIdx].Lock() //we only read the table
	defer book.Mu[dataIdx].Unlock()

	k, b := book.findIndex(dataIdx, id)
	if b {
		copy(book.Data[dataIdx][k:], book.Data[dataIdx][k+1:])
		book.Data[dataIdx] = book.Data[dataIdx][:len(book.Data[dataIdx])-1]
	}

	return b
}

//BitmexInsert implements OrderBookHandler
func (book *SimpleOB) BitmexInsert(isSell bool, id uint64, size uint32, price float32) bool {
	dataIdx := isSellToInt(isSell)
	book.Mu[dataIdx].Lock()
	defer book.Mu[dataIdx].Unlock()

	k, b := book.FindPrice(isSell, price)
	if b { //if already price there, we failed
		return false
	}

	book.Data[dataIdx] = append(book.Data[dataIdx], PriceLevel{0, 0, 0})
	copy(book.Data[dataIdx][k+1:], book.Data[dataIdx][k:])
	book.Data[dataIdx][k] = PriceLevel{id, size, price}

	return true
}

//BitmexFill implements OrderBookHandler
func (book *SimpleOB) BitmexFill(levels []OBUpdate) error {
	sells := make([]PriceLevel, 0, len(levels))
	buys := make([]PriceLevel, 0, len(levels))

	for _, v := range levels {
		if v.Side == "Sell" {
			sells = append(sells, PriceLevel{v.ID, v.Size, v.Price})
		} else if v.Side == "Buy" {
			buys = append(buys, PriceLevel{v.ID, v.Size, v.Price})
		} else {
			return fmt.Errorf("could not determine a level side: %v", v)
		}
	}

	var wg sync.WaitGroup

	wg.Add(2)
	go func() {
		sort.Slice(sells, func(i, j int) bool {
			return sells[i].Price > sells[j].Price
		})
		wg.Done()
	}()

	go func() {
		sort.Slice(buys, func(i, j int) bool {
			return buys[i].Price < buys[j].Price
		})
		wg.Done()
	}()
	wg.Wait()

	book.Data[isSellToInt(true)] = sells
	book.Data[isSellToInt(false)] = buys
	return nil
}
